angular.module('workshop', ['ngRoute']).config(function ($routeProvider) {
    $routeProvider
        .when('/pag1', {
            controller: 'pag1Controller',
            templateUrl: 'pag1.html'
        })
        .when('/pag2/:nombre?', {
            controller: 'pag2Controller',
            templateUrl: 'pag2.html'
        })
        .otherwise('/pag1');
});