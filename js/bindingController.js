angular.module('workshop').controller('bindingController', function ($scope) {
    $scope.sayMyName = function () {
        $scope.name = 'Heisenberg';
    };
    $scope.alert = function () {
        window.alert($scope.name);
    };

    $scope.items=[3,'carlos',new Date()]
});
