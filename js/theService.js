angular.module('workshop').service('theService', function () {
    var aName = 'Carlos';
    this.setName = function (name) {
        aName = name;
    };
    this.getName = function () {
        return aName;
    };
});